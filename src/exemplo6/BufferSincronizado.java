/**
 * UnsynchronizedBuffer
 * 
 * Autor: Luiz Alberto da Silva
 * Ultima modificacao: 16/08/2017
 */
package exemplo6;

import java.util.concurrent.ArrayBlockingQueue;

public class BufferSincronizado implements Buffer {

    //Buffer sincronizado
    private final ArrayBlockingQueue<Integer> buffer;
        
    public BufferSincronizado(){
        buffer = new ArrayBlockingQueue<Integer>(1);
    }
        
    public void set (int valor ) throws InterruptedException{
    
        //Bloqueia a execução da thread ateh haver espaco no buffer para gravar dados
        buffer.put(valor);
        System.out.println("Produtor escreve: " + valor + " Indice buffer: " + buffer.size());
    };
    
    public int get () throws InterruptedException{
        
        //Bloqueia a execução da thread enquanto nao existir valor no array 
        int lerValor = buffer.take(); //remove valor do buffer
        
        System.out.println("Consumidor lê   : " + lerValor + " Indice buffer: " + buffer.size());                
        
        return lerValor;
    }
    
}
