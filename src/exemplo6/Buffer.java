/**
 * Buffer
 * 
 * Autor: Luiz Alberto da Silva
 * Ultima modificacao: 16/08/2017
 */
package exemplo6;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
