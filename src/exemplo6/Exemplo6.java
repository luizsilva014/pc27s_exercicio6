/**
 * Exemplo de Produtor/Consumidor
 * sem sincronizacao
 * 
 * Autor: Luiz Alberto da Silva
 * Ultima modificacao: 16/08/2017
 */
package exemplo6;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Exemplo6  {

    
    public static void main(String [] args){
        
        //cria novo pool de threads com duas threads
        ExecutorService pool = Executors.newCachedThreadPool();
        
        //cria UnsynchronizedBuffer para armazenar ints
        int num = 2;
        Buffer [] buffer = new BufferSincronizado[num];
        for(int i=0; i<num; i++){
            buffer[i] = new BufferSincronizado();
        }
        
        System.out.println("Ação\t\tValor\tIndiceBuffer\tValor");;
        System.out.println("-------\t\t-------\t--------------\t------------\n");
        
        //Executa Producer e Consumer, fornecendo-lhes acesso ao mesmo Buffer->UnsynchronizedBuffer: sharedLocation
        pool.execute( new Producer( buffer ));        
        
        for(int i=0; i<num; i++){
            pool.execute( new Consumer( buffer, i ) );
        }
                    
        pool.shutdown();
    }//fim main
    
}//fim classe
